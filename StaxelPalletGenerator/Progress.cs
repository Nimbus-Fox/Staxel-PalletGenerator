﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Plukit.Base;
using Staxel.Voxel;
using StaxelPalletGenerator;

namespace StaxelPaletteGenerator {
    public partial class Progress : Form {
        public Progress() {
            InitializeComponent();
        }

        public void Start(string dir) {
            prgrssTemplate.Maximum = Form1._templates.Count;
            prgrssTemplate.Value = 0;

            var templateIndex = 1;

            foreach (var template in Form1._templates) {
                prgrssTemplate.Value = templateIndex;
                Application.DoEvents();
                lblTemplate.Text = $"{template.Key} ({templateIndex}/{Form1._templates.Count})";

                var buffer = File.ReadAllBytes(Path.Combine(dir, template.Value.GetString("qb")));

                var voxels =
                    VoxelLoader.LoadQb(
                        new MemoryStream(buffer, 0, buffer.Length, false, true),
                        template.Value.GetString("qb"), Vector3I.Zero, Vector3I.MaxValue);

                var orig = voxels.ColorData.ToArray();

                var paletteIndex = 1;
                var paletteCount = template.Value.GetList("palettes").Count;

                foreach (var palette in template.Value.GetList("palettes")) {
                    prgrssPalette.Value = paletteIndex;
                    Application.DoEvents();
                    voxels.ColorData = orig.ToArray();
                    lblPalette.Text = $"{palette.GetString()} ({paletteIndex}/{paletteCount})";
                    Application.DoEvents();

                    for (var i = 0; i < voxels.ColorData.Length; i++) {
                        foreach (var colors in Form1._palettes[palette.GetString()]) {
                            var color = voxels.ColorData[i];
                            if (color.R == colors.Key.R && color.G == colors.Key.G && color.B == colors.Key.B) {
                                voxels.ColorData[i] = colors.Value;
                                break;
                            }
                        }
                    }

                    var file = template.Value.GetString("qbFile");

                    if (file.Contains("$palette")) {
                        file = file.Replace("$palette", palette.GetString());
                    }

                    using (var stream = new FileStream(Path.Combine(dir, file), FileMode.Create)) {
                        voxels.SaveQB(stream);

                        stream.Flush(true);
                    }

                    file = template.Value.GetString("file");

                    if (file.Contains("$palette")) {
                        file = file.Replace("$palette", palette.GetString());
                    }

                    var content = template.Value.FetchBlob("content").Clone();

                    BlobLoop(palette.GetString(), content);

                    var jsonBuffer = new MemoryStream();

                    content.SaveJsonStream(jsonBuffer);

                    File.WriteAllBytes(Path.Combine(dir, file), jsonBuffer.ToArray());

                    Blob.Deallocate(ref content);

                    jsonBuffer.Dispose();

                    paletteIndex++;
                }

                templateIndex++;
            }
        }

        private void BlobLoop(string palette, Blob blob) {
            foreach (var entry in blob.KeyValueIteratable) {
                if (entry.Value.Kind == BlobEntryKind.Blob) {
                    BlobLoop(palette, entry.Value.Blob());
                }

                if (entry.Value.Kind == BlobEntryKind.String) {
                    var str = entry.Value.GetString();

                    if (str.Contains("$palette")) {
                        entry.Value.SetString(str.Replace("$palette", palette));
                    }
                }
            }
        }
    }
}
