﻿using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Microsoft.WindowsAPICodePack.Dialogs;
using Plukit.Base;
using Staxel.Core;
using Staxel.Voxel;
using StaxelPaletteGenerator;
using Color = Microsoft.Xna.Framework.Color;

namespace StaxelPalletGenerator {
    public partial class Form1 : Form {
        public Form1() {
            InitializeComponent();

            lstPalettes.Clear();
            lstPalettes.Columns.Add("colName", "Name", lstTemplates.Size.Width / 2);
            lstPalettes.Columns.Add("colPalettes", "Replacements", lstTemplates.Size.Width / 2);
            _palettes.Clear();

            lstTemplates.Clear();
            lstTemplates.Columns.Add("colTemplate", "Template", lstTemplates.Size.Width / 2);
            lstTemplates.Columns.Add("colPalettes", "Palettes");

            foreach (var t in _templates) {
                var blob = t.Value;
                Blob.Deallocate(ref blob);
            }

            _templates.Clear();
        }

        internal static readonly Dictionary<string, Dictionary<Color, Color>> _palettes = new Dictionary<string, Dictionary<Color, Color>>();

        internal static readonly Dictionary<string, Blob> _templates = new Dictionary<string, Blob>();

        private void btnBrowse_Click(object sender, System.EventArgs e) {
            var dir = new CommonOpenFileDialog {IsFolderPicker = true};

            if (dir.ShowDialog() == CommonFileDialogResult.Ok && !string.IsNullOrWhiteSpace(dir.FileName)) {
                txtDir.Text = dir.FileName;
            }
        }

        private void btnScan_Click(object sender, System.EventArgs e) {
            var dir = new DirectoryInfo(txtDir.Text);

            lstPalettes.Clear();
            lstPalettes.Columns.Add("colName", "Name", lstTemplates.Size.Width / 2);
            lstPalettes.Columns.Add("colPalettes", "Replacements");
            _palettes.Clear();

            lstTemplates.Clear();
            lstTemplates.Columns.Add("colTemplate", "template", lstTemplates.Size.Width / 2);
            lstTemplates.Columns.Add("colPalettes", "Palettes");

            foreach (var t in _templates) {
                var blob = t.Value;
                Blob.Deallocate(ref blob);
            }

            _templates.Clear();

            if (!dir.Exists) {
                MessageBox.Show("Directory does not exist", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var files = dir.GetFiles();
            var palettes = files.Where(x => x.Exists && x.FullName.ToLower().EndsWith(".palettes"));
            var templates = files.Where(x => x.Exists && x.FullName.ToLower().EndsWith(".template"));

            foreach (var palette in palettes) {
                var blob = BlobAllocator.Blob(true);
                using (var stream = palette.OpenRead()) {
                    using (var sr = new StreamReader(stream)) {
                        stream.Seek(0L, SeekOrigin.Begin);
                        blob.ReadJson(sr.ReadToEnd());
                    }
                }

                foreach (var entry in blob.KeyValueIteratable) {
                    if (entry.Value.Kind != BlobEntryKind.Blob) {
                        continue;
                    }

                    if (!_palettes.ContainsKey(entry.Key)) {
                        _palettes.Add(entry.Key, new Dictionary<Color, Color>());
                    }

                    var colorBlob = entry.Value.Blob();

                    foreach (var colorEntry in colorBlob.KeyValueIteratable) {
                        try {
                            if (!_palettes[entry.Key].ContainsKey(ColorMath.ParseString(colorEntry.Key))) {
                                _palettes[entry.Key].Add(ColorMath.ParseString(colorEntry.Key),
                                    colorEntry.Value.GetColor());
                            }
                        } catch {
                            // ignore
                        }
                    }
                }
            }

            foreach (var palette in _palettes) {
                var item = new ListViewItem {Text = palette.Key};

                item.SubItems.Add(palette.Value.Count.ToString());

                lstPalettes.Items.Add(item);
            }

            foreach (var template in templates) {
                var blob = BlobAllocator.Blob(true);
                using (var stream = template.OpenRead()) {
                    using (var sr = new StreamReader(stream)) {
                        stream.Seek(0L, SeekOrigin.Begin);
                        blob.ReadJson(sr.ReadToEnd());
                    }
                }

                _templates.Add(template.Name, blob);

                try {
                    var item = new ListViewItem {Text = template.Name};
                    item.SubItems.Add(blob.GetList("palettes").Count(x => _palettes.ContainsKey(x.GetString()))
                        .ToString());

                    lstTemplates.Items.Add(item);

                    
                } catch {
                    // ignore
                }
            }
        }

        private void btnGenerate_Click(object sender, System.EventArgs e) {
            var run = new Progress();

            run.Start(txtDir.Text);

            run.Dispose();

            MessageBox.Show("Palettes Generated");
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            System.Diagnostics.Process.Start("https://gitlab.com/Nimbus-Fox/Staxel-PalletGenerator");
        }
    }
}
