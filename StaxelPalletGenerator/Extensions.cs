﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Plukit.Base;
using Staxel;
using Staxel.Core;
using Staxel.Voxel;

namespace StaxelPalletGenerator {
    public static class Extensions {
        public static void SaveQB(this VoxelObject qb, Stream stream) {
            stream.WriteByte(1);
            stream.WriteByte(1);
            stream.WriteByte(0);
            stream.WriteByte(0);

            stream.WriteUInt(0);
            stream.WriteUInt(0);
            stream.WriteUInt(1);
            stream.WriteUInt(0);
            stream.WriteUInt(1);

            stream.WriteString("main");

            stream.WriteInt(qb.Dimensions.X);
            stream.WriteInt(qb.Dimensions.Y);
            stream.WriteInt(qb.Dimensions.Z);
            stream.WriteInt(0);
            stream.WriteInt(0);
            stream.WriteInt(0);

            void Action(Color color, int run) {
                if (run <= 0) {
                    return;
                }

                var num = 1;
                if (run > 2) {
                    stream.WriteUInt(2);
                    stream.WriteUInt((uint)run);
                } else {
                    num = run;
                }

                for (var index = 0; index < num; index++) {
                    stream.WriteUInt(color.PackedValue);
                }
            }

            var color1 = Color.White;
            var num2 = 0;

            for (var num1 = 0; num1 < qb.Dimensions.Z; num1++) {
                for (var index = 0; index < qb.Dimensions.X * qb.Dimensions.Y; index++) {
                    var num3 = index % qb.Dimensions.X;
                    var num4 = index / qb.Dimensions.X;

                    var transparent =
                        qb.ColorData[num3 + num1 * qb.Dimensions.X + num4 * qb.Dimensions.X * qb.Dimensions.Z];
                    if (((int)transparent.PackedValue & -16777216) == 0) {
                        transparent = Color.Transparent;
                    } else {
                        transparent.PackedValue |= 4278190080U;
                    }

                    if (transparent != color1) {
                        Action(color1, num2);
                        num2 = 0;
                    }

                    color1 = transparent;
                    num2++;
                }
                Action(color1, num2);
                num2 = 0;
                stream.WriteUInt(6);
            }
        }

        public static Color GetColor(this Blob blob, string key) {
            if (!blob.Contains(key)) {
                return Color.White;
            }

            return blob.KeyValueIteratable[key].GetColor();
        }

        public static Color GetColor(this BlobEntry entry) {
            if (entry.Kind == BlobEntryKind.Blob) {
                var current = entry.Blob();
                var r = current.GetLong("R", current.GetLong("r", Color.White.R));
                var g = current.GetLong("G", current.GetLong("g", Color.White.G));
                var b = current.GetLong("B", current.GetLong("b", Color.White.B));
                var a = current.GetLong("A", current.GetLong("a", Color.White.A));

                return new Color(r, g, b, a);
            }

            if (entry.Kind == BlobEntryKind.Int) {
                return uint.TryParse(entry.GetLong().ToString(), out var col) ? ColorMath.FromRgba(col) : Color.Transparent;
            }

            if (entry.Kind == BlobEntryKind.String) {
                return ColorMath.ParseString(entry.GetString());
            }

            return Color.White;
        }
    }
}
