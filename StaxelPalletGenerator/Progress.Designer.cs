﻿namespace StaxelPaletteGenerator {
    partial class Progress {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.lblTemplate = new System.Windows.Forms.Label();
            this.prgrssTemplate = new System.Windows.Forms.ProgressBar();
            this.lblPalette = new System.Windows.Forms.Label();
            this.prgrssPalette = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // lblTemplate
            // 
            this.lblTemplate.AutoSize = true;
            this.lblTemplate.Location = new System.Drawing.Point(9, 9);
            this.lblTemplate.Name = "lblTemplate";
            this.lblTemplate.Size = new System.Drawing.Size(35, 13);
            this.lblTemplate.TabIndex = 0;
            this.lblTemplate.Text = "label1";
            // 
            // prgrssTemplate
            // 
            this.prgrssTemplate.Location = new System.Drawing.Point(12, 30);
            this.prgrssTemplate.Name = "prgrssTemplate";
            this.prgrssTemplate.Size = new System.Drawing.Size(317, 23);
            this.prgrssTemplate.TabIndex = 1;
            // 
            // lblPalette
            // 
            this.lblPalette.AutoSize = true;
            this.lblPalette.Location = new System.Drawing.Point(9, 66);
            this.lblPalette.Name = "lblPalette";
            this.lblPalette.Size = new System.Drawing.Size(35, 13);
            this.lblPalette.TabIndex = 2;
            this.lblPalette.Text = "label1";
            // 
            // prgrssPalette
            // 
            this.prgrssPalette.Location = new System.Drawing.Point(12, 82);
            this.prgrssPalette.Name = "prgrssPalette";
            this.prgrssPalette.Size = new System.Drawing.Size(317, 23);
            this.prgrssPalette.TabIndex = 3;
            // 
            // Progress
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(341, 131);
            this.ControlBox = false;
            this.Controls.Add(this.prgrssPalette);
            this.Controls.Add(this.lblPalette);
            this.Controls.Add(this.prgrssTemplate);
            this.Controls.Add(this.lblTemplate);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Progress";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Progress";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTemplate;
        private System.Windows.Forms.ProgressBar prgrssTemplate;
        private System.Windows.Forms.Label lblPalette;
        private System.Windows.Forms.ProgressBar prgrssPalette;
    }
}